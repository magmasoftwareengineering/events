<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 06/02/2016
 * Time: 12:56
 */

namespace MagmaSoftwareEngineering\Events;


use League\Flysystem\Filesystem;
use Sabre\VObject\InvalidDataException;
use Sabre\VObject\Node;
use Sabre\VObject\ParseException;
use Sabre\VObject\Reader;

/**
 * Class LoadIcal
 *
 * https://www.gov.uk/bank-holidays
 *
 * @package MagmaSoftwareEngineering\Dates
 */
class LoadIcal
{

    /**
     * @var string
     */
    private $source = 'https://www.gov.uk/bank-holidays/england-and-wales.ics';

    /**
     * @var array
     */
    private $events = [];

    /**
     * @var null|string
     */
    private $uri = '';

    /**
     * @var null|\League\Flysystem\Filesystem
     */
    private $cache;

    /**
     * @param string     $uri You can pass a URL for a .ics file containing events
     *                        this defaults to the UK govt holiday data for England and Wales
     * @param Filesystem $cache
     */
    public function __construct($uri = '', Filesystem $cache = null)
    {

        if ('' !== $uri) {
            $this->setUri($uri);
        }

        if (null !== $cache) {
            $this->setCache($cache);
        }
    }

    /**
     * Process the iCal file
     *
     * @return $this
     * @throws InvalidDataException
     */
    public function process()
    {

        $events = $errors = [];

        try {
            $vcalendar = Reader::read(
                $this->fetch(),
                Reader::OPTION_FORGIVING & Reader::OPTION_IGNORE_INVALID_LINES
            );

            $errors = $vcalendar->validate(Node::PROFILE_CALDAV & Node::REPAIR);
        } catch (ParseException $e) {
            echo $e->getMessage();
        }

        if (null !== $vcalendar && 0 === count($errors)) {

            foreach ($vcalendar->VEVENT as $event) {
                $holiday = \DateTime::createFromFormat('Ymd', (string)$event->DTSTART);
                if ($holiday) {
                    $formatted = $holiday->format('Y-m-d');
                    $events[$formatted] = [
                        'summary' => (string)$event->SUMMARY,
                        'timestamp' => (string)$event->DTSTAMP
                    ];
                }
            }
        } else {
            $msg = '';
            foreach ($errors as $error) {
                $msg .= $error['message'] . ' ';
            }
            $msg .= $this->uri . ' ';
            $msg .= $this->fetch();
            throw new InvalidDataException('Invalid data: ' . $msg);
        }

        $this->events = $events;

        return $this;
    }

    /**
     * @param $cache
     *
     * @return $this
     */
    public function setCache(Filesystem $cache)
    {

        $this->cache = $cache;

        return $this;
    }

    /**
     * @param $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {

        $this->uri = $uri;

        return $this;
    }

    /**
     * @return array
     */
    public function getEvents()
    {

        return $this->events;
    }

    /**
     * @return bool|false|string
     */
    private function fetch()
    {

        $fileContents = '';
        $dirty = false;

        if (null === $this->uri || '' === $this->uri) {
            $this->uri = $this->source;
        }

        $parsed = parse_url($this->uri);
        $file = basename($parsed['path']);

        // retrieve from cache
        if ($this->cache && $this->cache->has($file)) {
            $fileContents = $this->cache->read($file);
        }

        if (null === $fileContents || '' === $fileContents) {
            $fileContents = $this->fetchByURI();
            $dirty = true;
        }

        // store to cache
        if ($this->cache && $dirty) {
            $this->cache->put($file, $fileContents);
        }

        return $fileContents;
    }

    /**
     * @return string
     */
    private function fetchByURI()
    {

        if (null === $this->uri || '' === $this->uri) {
            $this->uri = $this->source;
        }

        $chandle = curl_init($this->uri);
        curl_setopt($chandle, CURLOPT_RETURNTRANSFER, true);
        $ics = curl_exec($chandle);
        curl_close($chandle);

        return (string)$ics;
    }
}
